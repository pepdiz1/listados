---
layout: post
section: associations
title: Oficina de Software Libre de la Asociación de Universitarios Informáticos de la Universidad de Almería
---

## Información

-   Universidad a la que está asociada: Universidad de Almería
-   Activa en la actualidad: No

## Contacto

-   Sede física:
-   Página web: <http://asociacion-unia.es/>
-   Email:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/unia_ual>
-   Sala en Matrix:
-   Grupo o canal en Telegram:
-   GitLab/GitHub (u otros sitios de código colaborativo):
