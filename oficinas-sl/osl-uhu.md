---
layout: post
section: offices
title: Oficina de Software Libre y Conocimiento Abierto de la Universidad de Huelva
---

## Información

-   Director/a:
-   Email:
-   Activa en la actualidad: Parcialmente

## Contacto

-   Dirección física:
-   Página web: <http://www.uhu.es/osl/>
-   Email:
-   Teléfono:
-   Mastodon (u otras redes sociales libres):
-   Twitter: <https://twitter.com/osl_uhu>
-   Sala en Matrix:
-   Grupo o canal en Telegram:
-   GitLab/GitHub (u otros sitios de código colaborativo):
