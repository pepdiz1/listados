---
layout: post
section: offices
title: Oficina per al Programari Lliure de la Universitat Autònoma de Barcelona
---

## Información

-   Director/a:
-   Email:
-   Activa en la actualidad: No

## Contacto

-   Dirección física:
-   Página web: <https://pagines.uab.cat/opl/content/inici>
-   Email:
-   Teléfono:
-   Mastodon (u otras redes sociales libres):
-   Twitter:
-   Sala en Matrix:
-   Grupo o canal en Telegram:
-   GitLab/GitHub (u otros sitios de código colaborativo):
